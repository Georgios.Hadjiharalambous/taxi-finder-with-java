A project created to find a taxi in a certain town of Greece (Athens).<br/> Could be used for everywhere if the corresponding map coordinates are used in the file nodes.csv . 
<br/>
The taxis-coordinates available are located at the taxis.csv and the customer coordinates in the client.csv.<br/>
There could be as many taxis as you want, but only one customer.<br/>
It is implemented in Java, with the use of A* algorithm with beam search.<br/>
The size of beam search is given as an input by the user.<br/>
The complexity is o(n^2) as we did't used hash tables for simplicity.<br/>
Created by [Georgios Hadjiharalambous](https://gitlab.com/Georgios.Hadjiharalambous) and [Andreas Christou](https://github.com/christouandr7).